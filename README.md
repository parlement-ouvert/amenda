# Amenda

## Quick start

1. Install Docker
2. Install Docker Compose
3. Clone the repository: `git clone git@framagit.org:parlement-ouvert/amenda.git && cd amenda`
4. Start the services: `docker-compose up -d`
5. Edit your `hosts` file to add `127.0.0.1	amenda.fr.test`
6. In your Web browser, go to https://amenda.fr.test/gitlab/profile/personal_access_tokens (you will have to login first, the default admin credentials are in `docker-compose.yml`)
7. Create a new Personal Access Token with the `api` scope, keep the token somewhere because you'll need it later
8. Go to https://amenda.fr.test/gitlab/admin/applications
9. Click on "New application" to create a new Application with the following settings:
    * Name: Amenda
    * Redirect URI: https://amenda.fr.test/api/v1/auth/gitlab/callback
    * Trusted: checked
    * Scopes: `api`, `read_user`, `read_repository`, `write_repository`, `openid`, `profile`, `email`
9. Create the `api/config/local.toml` file with the following content:

```toml
[auth]
clientID = "client ID for the app created at step 9"
clientSecret = "client secret for the app created at step 9"

[gitlab]
token = "token created at step 7"
```

10. Restart the API: `docker-compose restart api`
11. Go to https://amenda.fr.test/api/v1/graphql to use the GraphQL playground
