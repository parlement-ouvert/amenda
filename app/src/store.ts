import { createStore } from "redux";
import { combineReducers } from "redux";
import { connectRouter } from 'connected-react-router'
import { History } from "history";

export default (history: History) => createStore(combineReducers({
    router: connectRouter(history),
}));
