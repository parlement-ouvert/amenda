import 'reflect-metadata';
import { ApolloServer } from "apollo-server-express";
import * as path from "path";
import { buildSchema, ResolverData } from "type-graphql";
import passport from "passport";
import config from "config";
import { BillResolver } from "./bill-resolver";
import { ClauseResolver } from "./clause-resolver";
import { Strategy as GitLabStrategy } from "passport-gitlab2";
import express from "express";
import cookieParser from "cookie-parser";
import cookieSession from "cookie-session";
import requestId from "express-request-id";
import jwt from "jsonwebtoken";
import { Container, ContainerInstance } from "typedi";
import { Gitlab } from "gitlab";

// ! FIXME: in production, generate a secured random string
const JWT_SECRET = 'secret';

export interface Context {
    requestId: number;
    user: Object,
    container: ContainerInstance;
}

passport.use(new GitLabStrategy({
    baseURL: config.get('auth.baseURL'),
    tokenURL: config.get('auth.tokenURL'),
    authorizationURL: config.get('auth.authorizationURL'),
    profileURL: config.get('auth.profileURL'),
    clientID: config.get('auth.clientID'),
    clientSecret: config.get('auth.clientSecret'),
    callbackURL: `${config.get('url')}/api/v1/auth/gitlab/callback`,
    scope: 'api,read_repository',
  },
  (accessToken, refreshToken, profile, callback) => {
    let user = {
        accessToken,
        username: profile.username,
        displayName: profile.displayName,
        id: profile.id,
    };

    callback(null, user);
  }
));

passport.serializeUser(function(user, done) {
    done(null, jwt.sign(user, JWT_SECRET));
});

passport.deserializeUser(function(token, done) {
    try {
        done(null, jwt.verify(token, JWT_SECRET));
    } catch (err) {
        done(err, null);
    }
});

const app = express();

async function bootstrap() {
  // build TypeGraphQL executable schema
  try {

    const schema = await buildSchema({
        resolvers: [ClauseResolver, BillResolver],
        // automatically create `schema.gql` file with schema definition in current folder
        emitSchemaFile: path.resolve(__dirname, '/tmp/schema.gql'),
        authChecker: ({ root, args, context, info }, roles) => {
            return !!context.user;
        },
        // The container will be dynamically created for each request because
        // some dependencies (ex: the Gitlab service) require session data.
        container: (({ context }: ResolverData<Context>) => context.container),
    });

    // Create GraphQL server
    const server = new ApolloServer({
        schema,
        context: ({ req }) => {
            const requestId = Math.floor(Math.random() * Number.MAX_SAFE_INTEGER);
            const container = Container.of(requestId);
            const context = {
                requestId,
                container,
                user: req['user'], // `req.user` comes from `express-jwt`
            };

            container.set('gitlab', new Gitlab({
                host: config.get('gitlab.url'),
                token: !!req['user'] ? null : config.get('gitlab.token'),
                oauthToken: !!req['user'] ? req['user']['accessToken'] : null,
            }));

            return context;
        },
        playground: {
            endpoint: `${config.get('url')}/api/v1/graphql`,
            settings: {
                'request.credentials': 'include',
            },
        },
    });

    app.use(cookieParser());
    app.use(cookieSession({
        // ! FIXME: use proper values in production
        keys: ['secret'],
        secret: 'secret',
        resave: false,
        saveUninitialized: true,
    }));
    app.use(passport.initialize());
    app.use(passport.session());
    app.use(requestId());
    app.get('/ping', (req, res) => {
        res.send('pong');
    });
    app.get('/auth/gitlab', passport.authenticate('gitlab'));
    app.get(
        '/auth/gitlab/callback',
        passport.authenticate('gitlab', { failureRedirect: '/login' }),
        function(_req, res) {
            // Successful authentication, redirect home.
            res.redirect('/');
        }
    );

    server.applyMiddleware({ app, path: '/graphql' });

    // Launch the express server
    app.listen({ port: 4000 }, () =>
        console.log(`🚀 Server ready at ${config.get('url')}/api/v1/graphql`),
    );
    } catch (e) {
        console.error(e);
    }
}

bootstrap();
