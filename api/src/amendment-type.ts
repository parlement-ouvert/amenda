import { Field, ObjectType } from "type-graphql";

@ObjectType()
export class Amendment {
  @Field(type => String)
  patch: string;

  public static fromGitlabDiff(diff: Object): Amendment {
    let amendment = new Amendment();

    amendment.patch = diff['diff'];

    return amendment;
  }
}
