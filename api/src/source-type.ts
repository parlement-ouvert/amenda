import { Field, ObjectType } from "type-graphql";
import tmp from "tmp";
import slugify from "slugify";
import path from "path";
import fs from "fs";
import toml from "@iarna/toml";
import simplegit from "simple-git/promise";

@ObjectType({ description: 'Object representing a bill source' })
export class Source {
  @Field(type => String)
  name: string;

  @Field(type => String)
  repository: string;

  @Field(type => String)
  ref: string;

  public constructor(name: string, repository: string, ref: string) {
    this.name = name;
    this.repository = repository;
    this.ref = ref;
  }

  public async addToRepository(repository: string, branch: string = 'master'): Promise<boolean> {
    try {
      const tmpDir = tmp.dirSync({keep: process.env.NODE_ENV === 'development'}).name;
      // The dev env uses self-signed certificates.
      const sslVerify = process.env.NODE_ENV === 'development' ? 'false' : 'true';
      const source_subtree_prefix = slugify(this.name).toLowerCase();
      const bill_toml_path = path.join(tmpDir, 'bill.toml');

      // ? It's probably unsafe to check the dependencies outside of the
      // ? actual git repo context/history: when we clone the repo, the
      // ? file might have been changed in the meantime.
      const git = simplegit();

      await git.raw(['config', '--global', 'http.sslverify', sslVerify]);
      await git.clone(repository, tmpDir);

      const repo = simplegit(tmpDir);

      await repo.checkout(branch);

      // Check if that source has not already been added for this bill.
      let bill_toml = toml.parse(fs.readFileSync(bill_toml_path, 'utf-8'));

      if (!!bill_toml['sources'][this.name] ||
          (Array.isArray(bill_toml['sources']) && 
            (bill_toml['sources'] as any[]).filter(s => s['git'] === repository).length != 0)) {
        return false;
      }

      bill_toml['sources'][this.name] = { git: this.repository, ref: this.ref };

      // ? FIXME: Does simple-git properly escape the arguments to avoid injections
      // ? and remote code execution?
      await repo.addConfig('user.name', 'Amenda API');
      await repo.addConfig('user.email', 'api@amenda.fr');
      await repo.raw([
        'subtree',
        'add',
        '--prefix=' + source_subtree_prefix,
        this.repository,
        this.ref,
      ]);
      await repo.add([source_subtree_prefix]);

      fs.writeFileSync(bill_toml_path, toml.stringify(bill_toml));

      await repo.commit(
        `Add the "${this.name}" source.`,
        [
          bill_toml_path,
          source_subtree_prefix,
        ]
      );
      await repo.push();

      return true;
    } catch (e) {
      console.log(e);
      return false;
    }
  }
}