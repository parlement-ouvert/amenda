import { Resolver, ResolverInterface, Arg, Query, Mutation, Int, FieldResolver, Root, Authorized, Ctx } from "type-graphql";
import config from "config";
import slugify from "slugify";
import path from "path";
import fs from "fs";
import { Service, Inject } from "typedi";
import toml from "@iarna/toml";

import { Bill } from "./bill-type";
import { Context } from "apollo-server-core";
import { Clause } from "./clause-type";
import { Source } from "./source-type";

@Service()
@Resolver(of => Bill)
export class BillResolver implements ResolverInterface<Bill> {
  constructor(
    @Inject('gitlab') private readonly gitlab,
  ) {}
  
  @Mutation(returns => Bill)
  async createBill(
    @Arg('name') name: string,
  ): Promise<Bill> {
    let bill = Bill.fromGitlabProject(
      await this.gitlab.Projects.create({
        name,
        path: config.get('gitlab.billProjectPathPrefix') + slugify(name).toLowerCase(),
      })
    );

    let files = [
      'bill.toml',
    ];

    await this.gitlab.Commits.create(
      bill.id,
      'master',
      'Initialize bill repository.',
      files.map(f => { return {
        action: 'create',
        filePath: f,
        content: fs.readFileSync(path.join('./template/bill', f), 'utf-8')
      } })
    );

    return bill;
  }

  @Mutation(returns => Boolean)
  async deleteBill(
    @Arg('id', type => Int) id: number,
  ): Promise<Boolean> {
    try {
      await this.gitlab.Projects.remove(id);

      return true;
    } catch {
      return false;
    }
  }

  @Authorized()
  @Mutation(returns => Boolean)
  async addBillSource(
    @Ctx() ctx: Context,
    @Arg('billId', type => Int) billId: number,
    @Arg('sourceName', type => String) sourceName: string,
    @Arg('sourceRepository', type => String) sourceRepository: string,
    @Arg('sourceRef', type => String) sourceRef: string,
  ): Promise<Boolean> {
    const bill = Bill.fromGitlabProject(await this.gitlab.Projects.show(billId));
    const source = new Source(sourceName, sourceRepository, sourceRef);

    // Add basic HTTP authentication to the repository URL.
    // ! Only HTTPS repository URIs are supported for now.
    const bill_repo_url = bill.repository.replace(/^https:\/\//, `https://oauth2:${ctx['user'].accessToken}@`);

    return source.addToRepository(bill_repo_url);
  }

  @Query(returns => [Bill])
  async getBills() {
    let prefix = config.get('gitlab.billProjectPathPrefix');
    let projects = await this.gitlab.Projects.all({ owned: true, search: prefix }) as any[];

    return projects
      .filter((p) => p['path'].startsWith(prefix))
      .map((p) => Bill.fromGitlabProject(p));
  }

  @Query(returns => Bill)
  async getBill(
    @Arg('billId', type => Int) billId: number,
  ) {
    return Bill.fromGitlabProject(await this.gitlab.Projects.show(billId));
  }

  @FieldResolver(returns => [Clause])
  async clauses(@Root() bill: Bill): Promise<Clause[]> {
    let mergeRequests = await this.gitlab.MergeRequests.all({ projectId: bill.id }) as any[];

    return mergeRequests.map(b => Clause.fromGitlabMergeRequest(b));
  }

  @FieldResolver(returns => [Source])
  async sources(@Root() bill: Bill): Promise<Source[]> {
    const bill_toml = toml.parse(await this.gitlab.RepositoryFiles.showRaw(bill.id, 'bill.toml', 'master'));
    const sources = bill_toml['sources'] as any;

    return Object.keys(sources).map(name => new Source(name, sources[name].git, sources[name].ref));
  }
}
