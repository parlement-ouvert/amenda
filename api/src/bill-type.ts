import { Field, ObjectType, Int } from "type-graphql";
import SHA256 from "crypto-js/sha256";

import { Clause } from "./clause-type";
import { Source } from "./source-type";

@ObjectType({ description: 'Object representing a bill' })
export class Bill {
  @Field(type => String, { description: 'The universally unique ID of the bill' })
  uid: string;

  @Field(type => Int)
  id: number;

  @Field(type => String)
  name: string;

  @Field(type => String)
  path: string;

  @Field(type => String)
  repository: string;

  @Field(type => String, {nullable: true})
  description: string;

  @Field(type => [Clause])
  clauses: Clause[];

  @Field(type => [Source])
  sources: Source[];

  public static fromGitlabProject(project: Object): Bill {
    let bill = new Bill();

    bill.uid = SHA256(project['web_url']).toString();
    bill.id = project['id'];
    bill.name = project['name'];
    bill.path = project['path'];
    bill.description = project['description'];
    bill.repository = project['http_url_to_repo'];
    
    return bill;
  }
}
