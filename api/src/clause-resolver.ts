import { Resolver, ResolverInterface, Arg, Mutation, Int, FieldResolver, Authorized, Ctx, Root } from "type-graphql";
import slugify from "slugify";
import { Service, Inject } from "typedi";
import { Context } from "apollo-server-core";
import toml from "@iarna/toml";

import { Clause, ClauseStatus } from "./clause-type";
import { Amendment } from "./amendment-type";
import { Bill } from "./bill-type";
import { Source } from "./source-type";

@Service()
@Resolver(of => Clause)
export class ClauseResolver implements ResolverInterface<Clause> {
  constructor(
    @Inject('gitlab') private readonly gitlab,
  ) {}

  @Mutation(returns => Clause)
  async createClause(
    @Arg('billId') billId: number,
    @Arg('name') name: string,
  ): Promise<Clause> {
    const branchName = slugify(name).toLowerCase();
    
    await this.gitlab.Branches.create(billId, branchName, 'master');
    
    const mergeRequest = await this.gitlab.MergeRequests.create(
      billId,
      branchName,
      'master',
      name,
      {
        labels: Clause.statusToLabel(ClauseStatus.DRAFT),
      }
    );

    return Clause.fromGitlabMergeRequest(mergeRequest);
  }

  @Authorized()
  @Mutation(returns => Boolean)
  async addClauseSource(
    @Ctx() ctx: Context,
    @Arg('billId', type => Int) billId: number,
    @Arg('clauseId', type => Number) clauseId: number,
    @Arg('sourceName', type => String) sourceName: string,
    @Arg('sourceRepository', type => String) sourceRepository: string,
    @Arg('sourceRef', type => String) sourceRef: string,
  ): Promise<Boolean> {
    const bill = Bill.fromGitlabProject(await this.gitlab.Projects.show(billId));
    const clause = Clause.fromGitlabMergeRequest(await this.gitlab.MergeRequests.show(billId, clauseId));
    const source = new Source(sourceName, sourceRepository, sourceRef);

    // Add basic HTTP authentication to the repository URL.
    // ! Only HTTPS repository URIs are supported for now.
    const bill_repo_url = bill.repository.replace(/^https:\/\//, `https://oauth2:${ctx['user'].accessToken}@`);

    return source.addToRepository(bill_repo_url, clause.path);
  }

  @FieldResolver(type => [Amendment])
  async amendments(@Root() clause: Clause): Promise<Amendment[]> {
    const diffs = await this.gitlab.Repositories.compare(
      clause.billId,
      'master',
      clause.path
    );

    return diffs['diffs'].map(diff => Amendment.fromGitlabDiff(diff));
  }

  @FieldResolver(returns => Bill)
  async bill(@Root() clause: Clause): Promise<Bill> {
    return Bill.fromGitlabProject(await this.gitlab.Projects.show(clause.billId));
  }

  @FieldResolver(returns => [Source])
  async sources(@Root() clause: Clause): Promise<Source[]> {
    const bill_toml = toml.parse(await this.gitlab.RepositoryFiles.showRaw(clause.billId, 'bill.toml', clause.path));
    const sources = bill_toml['sources'] as any;

    return Object.keys(sources).map(name => new Source(name, sources[name].git, sources[name].ref));
  }
}
