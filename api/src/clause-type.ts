import { Field, ObjectType, Int } from "type-graphql";
import SHA256 from "crypto-js/sha256";

import { Source } from "./source-type";

export enum ClauseStatus {
    UNKNOWN     = 'unknown',
    DRAFT       = 'draft',      // the clause is still being written
    PENDING     = 'pending',    // the clause is being voted
    APPROVED    = 'approved',   // the clause passed the vote
    REJECTED    = 'rejected',   // the clause did *not* pass the vote
}

// A map to go from a merge request label to an actual ClauseStatus.
// Must be ordered from lowest to highest priority.
let labelToStatus: Map<string, ClauseStatus> = new Map([
    ['status:draft',    ClauseStatus.DRAFT],
    ['status:pending',  ClauseStatus.PENDING],
    ['status:approved', ClauseStatus.APPROVED],
    ['status:rejected', ClauseStatus.REJECTED],
]);

@ObjectType({ description: 'Object representing a bill clause' })
export class Clause {
  @Field(type => String, { description: 'The universally unique ID of the clause' })
  uid: string;

  @Field(type => Int)
  id: number;

  @Field(type => Int)
  billId: number;

  @Field(type => String)
  name: string;

  @Field(type => String)
  status: ClauseStatus = ClauseStatus.UNKNOWN;

  @Field(type => String)
  path: string;

  @Field(type => [Source])
  sources: Source[];

  public static statusToLabel(status: ClauseStatus): string {
    return Array.from(labelToStatus.keys()).filter(k => labelToStatus.get(k) === status)[0]
  }

  public static fromGitlabMergeRequest(mergeRequest: Object): Clause {
    let clause = new Clause();

    clause.uid = SHA256(mergeRequest['web_url']).toString();
    clause.id = mergeRequest['id'];
    clause.billId = mergeRequest['project_id'];
    clause.name = mergeRequest['title'];
    clause.path = mergeRequest['source_branch'];

    // ! FIXME: log a warning when we have multiple status labels

    let statusLabel = Array.from(labelToStatus.keys())
        // In case we have (by mistake) multiple status labels on a single
        // merge request, we want to take the one with the 'highest' priority.
        // Since we'll take the first item in the filtered list, we sort the
        // labels from highest to lowest priority by calling reverse().
        .reverse()
        // Keep only the labels that actually match a status.
        .filter((k) => mergeRequest['labels'].indexOf(k) >= 0)
        // Take the first once.
        [0];

    clause.status = labelToStatus.get(statusLabel) || ClauseStatus.UNKNOWN;

    return clause;
  }
}
